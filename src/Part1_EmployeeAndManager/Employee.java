package Part1_EmployeeAndManager;
/**
 *
 * @author mitpa
 */
 class Employee 
{
    protected double wage;
    protected double hours;
    protected String name;

    public Employee(double wage, double hours, String name) {
        this.wage = wage;
        this.hours = hours;
        this.name = name;
    }

    public double getWage() {
        return wage;
    }

    public double getHours() {
        return hours;
    }

    public String getName() {
        return name;
    }
    double calculatePay()
    {
        return wage * hours;
    }    
}
