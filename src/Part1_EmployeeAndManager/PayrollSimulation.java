package Part1_EmployeeAndManager;
/**
 *
 * @author mitpa
 */
public class PayrollSimulation
{
    public static void main(String[]args)
    {
        Manager m1 = new Manager(17.50, 40, "Mit" , 250);
        Employee e1 = new Employee(18, 23, "Jerry" );
        
    System.out.println("Employee name =" +m1.name);
    System.out.println("Employee hours =" +m1.hours);
    System.out.println("Employee wage =" +m1.wage);
    System.out.println("Employee bonus =" +m1.bonus);
    
    System.out.println("PAY WITH Mit(manager) = " +m1.calculatePay());
     
    System.out.println("PAY WITH Jerry(employee) = " +e1.calculatePay());
    
    }   
}
