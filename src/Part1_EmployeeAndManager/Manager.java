package Part1_EmployeeAndManager;
/**
 *
 * @author mitpa
 */
public class Manager extends Employee
{
    protected double bonus;
    
      public Manager(double wage, double hours, String name,double bonus) 
    {
        super(wage,hours,name);
       this.bonus = bonus;
    }
    public double getBonus() {
        return bonus;
    }

    double calculatePay()
    {
        return super.calculatePay() + bonus;
    }
}
